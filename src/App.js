import { useEffect, useState } from "react";

function App() {
  const [quote, setQuote] = useState([]);
  const getQuote = () => {
    fetch("https://api.quotable.io/random")
      .then((response) => response.json())
      .then((data) => setQuote(data))
      .then(console.log(quote));
  };
  useEffect(() => {
    getQuote();
  }, []);

  return (
    <div className="card">
      <div className="text">
        <p>‘‘ {quote.content} ,,</p>
      </div>
      <div className="author">
        <h4>{quote.author}</h4>
      </div>

      <button className="btn" onClick={getQuote}>
        New Quote
      </button>
    </div>
  );
}

export default App;
